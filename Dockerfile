FROM node:alpine
# create app directory
WORKDIR /srv/app
# dependencies
COPY package*.json ./
RUN npm update
RUN npm install --only=production
#RUN npm install
# files
COPY server.js .
# EXPOSE 80
CMD [ "npm", "start" ] 
