'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const ICAL = require('ICAL');
const app = express();
const fs = require('fs');

// app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.set('port', (process.env.PORT || 80));
app.all('/', function(request, response) {
  console.log(JSON.stringify(request.body));
  console.log(JSON.stringify(request.query));
  console.log('hello world');
  response.send('hello world');
});

/** format JSON message to CSV line and append to data/chat_long.csv.
 * @param {string} message - Telegram JSON message */
function logMessage(message) {
  csvLine = message.date + '; ' + message.chat.username + '; ' +
    message.from.username + '; ';
  if (message.text) csvLine += JSON.stringify(message.text);
  else if (message.photo) {
    csvLine += 'photo: ' + message.photo[0].file_id;
  } else if (message.sticker) {
    csvLine += 'sticker: ' + message.sticker.set_name +
      ': ' + message.sticker.emoji;
  }
  csvLine += '\n';
  console.log(csvLine);
  fs.appendFile('/data/chat_log.csv', csvLine, function(err) {
    if (err) throw err;
    console.log('Saved!');
  });
}

csvHeader = 'Date; Chat; From; Text';
app.post('/telegram', function(request, response) {
  const {
    message,
  } = request.body;
  console.log(message);
  logMessage(message);
  response.write(JSON.stringify(request.body));
  response.end();
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
